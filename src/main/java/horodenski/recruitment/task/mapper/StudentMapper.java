package horodenski.recruitment.task.mapper;

import horodenski.recruitment.task.dto.StudentDto;
import horodenski.recruitment.task.model.Student;

public class StudentMapper {

    private StudentMapper(){}

    public static Student mapToStudent(StudentDto studentDto) {
        return Student.builder()
                .age(studentDto.getAge())
                .email(studentDto.getEmail())
                .major(studentDto.getMajor())
                .name(studentDto.getName())
                .surname(studentDto.getSurname())
                .build();
    }
}
