package horodenski.recruitment.task.mapper;

import horodenski.recruitment.task.dto.TeacherDto;
import horodenski.recruitment.task.model.Teacher;

public class TeacherMapper {

    private TeacherMapper(){}

    public static Teacher mapToTeacher(TeacherDto teacherDto) {
        return Teacher.builder()
                .age(teacherDto.getAge())
                .email(teacherDto.getEmail())
                .name(teacherDto.getName())
                .surname(teacherDto.getSurname())
                .subject(teacherDto.getSubject())
                .build();
    }
}
