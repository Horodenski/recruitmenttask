package horodenski.recruitment.task.repository;

import horodenski.recruitment.task.model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("Select c.students from Teacher c where c.id=:teacherId ")
    Page<Student> findAllByTeacher(@Param("teacherId") Long teacherId, Pageable paging);

    List<Student> findAllByName(String name);

    List<Student> findAllBySurname(String surname);

    List<Student> findAllByNameAndSurname(String name, String surname);
}
