package horodenski.recruitment.task.repository;

import horodenski.recruitment.task.model.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    @Query("Select c.teachers from Student c where c.id=:studentId")
    Page<Teacher> findAllByStudent(@Param("studentId") Long studentId, Pageable paging);

    List<Teacher>   findAllByName(String name);
    List<Teacher>   findAllBySurname(String surname);
    List<Teacher>   findAllByNameAndSurname(String name, String surname);
}
