package horodenski.recruitment.task.service;

import horodenski.recruitment.task.exception.TeacherNotFoundException;
import horodenski.recruitment.task.model.Student;
import horodenski.recruitment.task.model.Teacher;
import horodenski.recruitment.task.repository.TeacherRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TeacherService {
    private final TeacherRepository teacherRepository;


    public Page<Teacher> getAllTeachers(Pageable paging) {
        return teacherRepository.findAll(paging);
    }

    public Page<Teacher> getAllStudentTeachers(Long studentId, Pageable paging) {
        return teacherRepository.findAllByStudent(studentId, paging);
    }

    public Teacher createTeacher(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    public Optional<Teacher> getTeacherById(Long id) {
        return teacherRepository.findById(id);
    }

    public void updateTeacher(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    @Transactional
    public void removeTeacher(Long id) {
        Teacher teacher = getTeacher(id);
        teacher.getStudents().forEach(student -> student.removeTeacher(teacher));
        teacherRepository.delete(teacher);
    }

    @Transactional
    public void addStudentToTeacher(Student student, Long teacherId) {
        Teacher teacher = getTeacher(teacherId);

        student.addTeacher(teacher);
    }

    @Transactional
    public void removeStudentFromTeacher(Student student, Long teacherId) {
        Teacher teacher = getTeacher(teacherId);

        student.removeTeacher(teacher);
    }

    public Teacher getTeacher(Long teacherId) {
        return getTeacherById(teacherId).orElseThrow(
                () -> new TeacherNotFoundException("Teacher with id " + teacherId + " not exists")
        );
    }

    public List<Teacher> findTeachers(String name, String surname) {
        if (name != null && surname != null) {
            return findAllTeachersByNameAndSurname(name, surname);
        } else if (name != null) {
            return findAllTeachersByName(name);
        } else if (surname != null) {
            return findAllTeachersBySurname(surname);
        }
        return findAllTeachers();
    }

    private List<Teacher> findAllTeachers() {
        return teacherRepository.findAll();
    }

    private List<Teacher> findAllTeachersBySurname(String surname) {
        return teacherRepository.findAllBySurname(surname);
    }

    private List<Teacher> findAllTeachersByName(String name) {
        return teacherRepository.findAllByName(name);
    }

    private List<Teacher> findAllTeachersByNameAndSurname(String name, String surname) {
        return teacherRepository.findAllByNameAndSurname(name, surname);
    }
}
