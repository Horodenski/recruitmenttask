package horodenski.recruitment.task.service;

import horodenski.recruitment.task.exception.StudentNotFoundException;
import horodenski.recruitment.task.model.Student;
import horodenski.recruitment.task.model.Teacher;
import horodenski.recruitment.task.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public Page<Student> getAllStudents(Pageable paging) {
        return studentRepository.findAll(paging);
    }

    public Page<Student> getAllTeacherStudents(Long teacherId, Pageable paging) {
        return studentRepository.findAllByTeacher(teacherId, paging);
    }

    public Student createStudent(Student student) {
        return studentRepository.save(student);
    }

    public void updateStudent(Student student) {
        studentRepository.save(student);
    }

    public void removeStudent(Long id) {
        Student student = getStudent(id);
        studentRepository.delete(student);
    }


    public Optional<Student> getStudentById(Long id) {
        return studentRepository.findById(id);
    }

    @Transactional
    public void addTeacherToStudent(Long studentId, Teacher teacher) {
        Student student = getStudent(studentId);

        student.addTeacher(teacher);
    }

    @Transactional
    public void removeTeacherFromStudent(Long studentId, Teacher teacher) {
        Student student = getStudent(studentId);

        student.removeTeacher(teacher);
    }

    public Student getStudent(Long studentId) {
        return studentRepository.findById(studentId).orElseThrow(
                () -> new StudentNotFoundException("Student with id " + studentId + " not exists")
        );
    }

    public List<Student> findStudents(String name, String surname) {
        if (name != null && surname != null) {
            return findAllStudentsByNameAndSurname(name, surname);
        } else if (name != null) {
            return findAllStudentsByName(name);
        } else if (surname != null) {
            return findAllStudentsBySurname(surname);
        }
        return findAllStudents();
    }

    private List<Student> findAllStudents() {
        return studentRepository.findAll();
    }

    private List<Student> findAllStudentsBySurname(String surname) {
        return studentRepository.findAllBySurname(surname);
    }

    private List<Student> findAllStudentsByName(String name) {
        return studentRepository.findAllByName(name);
    }

    private List<Student> findAllStudentsByNameAndSurname(String name, String surname) {
        return studentRepository.findAllByNameAndSurname(name, surname);
    }
}
