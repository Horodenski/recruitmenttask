package horodenski.recruitment.task.controllers;

import horodenski.recruitment.task.dto.StudentDto;
import horodenski.recruitment.task.mapper.StudentMapper;
import horodenski.recruitment.task.model.Student;
import horodenski.recruitment.task.model.Teacher;
import horodenski.recruitment.task.service.StudentService;
import horodenski.recruitment.task.service.TeacherService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class StudentController {
    private final StudentService studentService;
    private final TeacherService teacherService;

    @PostMapping("/students")
    public ResponseEntity<Student> createStudent(@RequestBody StudentDto studentDto) {
        Student response = studentService.createStudent(StudentMapper.mapToStudent(studentDto));

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/students/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody StudentDto studentDto) {
        Optional<Student> idStudent = studentService.getStudentById(id);

        if (idStudent.isPresent()) {
            Student student = idStudent.get();
            student.setMajor(studentDto.getMajor());
            student.setEmail(studentDto.getEmail());
            student.setAge(studentDto.getAge());
            student.setSurname(studentDto.getSurname());
            student.setName(studentDto.getName());

            studentService.updateStudent(student);
            return ResponseEntity.noContent().build();
        }

        Student response = studentService.createStudent(StudentMapper.mapToStudent(studentDto));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @DeleteMapping("/students/{id}")
    public ResponseEntity<HttpStatus> deleteStudent(@PathVariable Long id) {
        studentService.removeStudent(id);

        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/students/{studentId}/addTeacher/{teacherId}")
    public ResponseEntity<HttpStatus> addTeacherToStudent(@PathVariable Long studentId, @PathVariable Long teacherId) {
        Teacher teacher = teacherService.getTeacher(teacherId);
        studentService.addTeacherToStudent(studentId, teacher);

        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/students/{studentId}/removeTeacher/{teacherId}")
    public ResponseEntity<HttpStatus> removeTeacherFromStudent(@PathVariable Long studentId, @PathVariable Long teacherId) {
        Teacher teacher = teacherService.getTeacher(teacherId);
        studentService.removeTeacherFromStudent(studentId, teacher);

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/students/find")
    public ResponseEntity<List<Student>> findTeacher(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String surname
    ) {
        List<Student> students = studentService.findStudents(name, surname);

        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @GetMapping("/students")
    public ResponseEntity<Map<String, Object>> getAllStudents(
            @RequestParam(required = false) Long teacherId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id,desc") String[] sort
    ) {
        List<Sort.Order> orders = new ArrayList<>();

        if (sort[0].contains(",")) {
            for (String sortOrder : sort) {
                String[] sortTable = sortOrder.split(",");
                orders.add(new Sort.Order(getSortDirection(sortTable[1]), sortTable[0]));
            }
        } else {
            orders.add(new Sort.Order(getSortDirection(sort[1]), sort[0]));
        }

        Pageable paging = PageRequest.of(page, size, Sort.by(orders));

        Page<Student> studentsPage;

        if (teacherId == null) {
            studentsPage = studentService.getAllStudents(paging);
        } else {
            studentsPage = studentService.getAllTeacherStudents(teacherId, paging);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("students", studentsPage.getContent());
        response.put("currentPage", studentsPage.getNumber());
        response.put("totalItems", studentsPage.getTotalElements());
        response.put("totalPages", studentsPage.getTotalPages());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }
}
