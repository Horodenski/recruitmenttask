package horodenski.recruitment.task.controllers;

import horodenski.recruitment.task.dto.TeacherDto;
import horodenski.recruitment.task.mapper.TeacherMapper;
import horodenski.recruitment.task.model.Student;
import horodenski.recruitment.task.model.Teacher;
import horodenski.recruitment.task.service.StudentService;
import horodenski.recruitment.task.service.TeacherService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class TeacherController {
    private final TeacherService teacherService;
    private final StudentService studentService;

    @PostMapping("/teachers")
    public ResponseEntity<Teacher> createTeacher(@RequestBody TeacherDto teacherDto) {
        Teacher response = teacherService.createTeacher(TeacherMapper.mapToTeacher(teacherDto));

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/teachers/{id}")
    public ResponseEntity<Teacher> updateTeacher(@PathVariable Long id, @RequestBody TeacherDto teacherDto) {
        Optional<Teacher> idTeacher = teacherService.getTeacherById(id);

        if (idTeacher.isPresent()) {
            Teacher teacher = idTeacher.get();
            teacher.setAge(teacherDto.getAge());
            teacher.setEmail(teacherDto.getEmail());
            teacher.setName(teacherDto.getName());
            teacher.setSubject(teacherDto.getSubject());
            teacher.setSurname(teacherDto.getSurname());

            teacherService.updateTeacher(teacher);
            return ResponseEntity.noContent().build();
        }

        Teacher response = teacherService.createTeacher(TeacherMapper.mapToTeacher(teacherDto));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @DeleteMapping("/teachers/{id}")
    public ResponseEntity<HttpStatus> deleteTeacher(@PathVariable Long id) {
        teacherService.removeTeacher(id);

        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/teachers/{teacherId}/addStudent/{studentId}")
    public ResponseEntity<HttpStatus> addStudentToTeacher(@PathVariable Long teacherId, @PathVariable Long studentId) {
        Student student = studentService.getStudent(studentId);
        teacherService.addStudentToTeacher(student, teacherId);

        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/teachers/{teacherId}/removeStudent/{studentId}")
    public ResponseEntity<HttpStatus> removeStudentFromTeacher(@PathVariable Long teacherId, @PathVariable Long studentId) {
        Student student = studentService.getStudent(studentId);
        teacherService.removeStudentFromTeacher(student, teacherId);

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/teachers/find")
    public ResponseEntity<List<Teacher>> findTeacher(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String surname
    ) {
        List<Teacher> teachers = teacherService.findTeachers(name, surname);

        return new ResponseEntity<>(teachers, HttpStatus.OK);
    }

    @GetMapping("/teachers")
    public ResponseEntity<Map<String, Object>> getAllTeachers(
            @RequestParam(required = false) Long studentId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id,desc") String[] sort
    ) {

        List<Order> orders = new ArrayList<>();

        if (sort[0].contains(",")) {
            for (String sortOrder : sort) {
                String[] sortTab = sortOrder.split(",");
                orders.add(new Order(getSortDirection(sortTab[1]), sortTab[0]));
            }
        } else {
            orders.add(new Order(getSortDirection(sort[1]), sort[0]));
        }

        Pageable paging = PageRequest.of(page, size, Sort.by(orders));

        Page<Teacher> teachersPage;

        if (studentId == null) {
            teachersPage = teacherService.getAllTeachers(paging);
        } else {
            teachersPage = teacherService.getAllStudentTeachers(studentId, paging);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("teachers", teachersPage.getContent());
        response.put("currentPage", teachersPage.getNumber());
        response.put("totalItems", teachersPage.getTotalElements());
        response.put("totalPages", teachersPage.getTotalPages());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }
}
