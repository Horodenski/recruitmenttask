package horodenski.recruitment.task.dto;

import horodenski.recruitment.task.model.Major;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentDto {
    private String name;
    private String surname;
    private Short age;
    private String email;
    private Major major;
}
