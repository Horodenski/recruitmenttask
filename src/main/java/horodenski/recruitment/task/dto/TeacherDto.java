package horodenski.recruitment.task.dto;

import horodenski.recruitment.task.model.Subject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeacherDto {
    private String name;
    private String surname;
    private Short age;
    private String email;
    private Subject subject;
}
