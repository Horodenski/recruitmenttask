package horodenski.recruitment.task.model;

public enum Subject {
    BIOLOGY,
    MATH,
    PHYSICS,
    COMPUTER_SCIENCE
}
