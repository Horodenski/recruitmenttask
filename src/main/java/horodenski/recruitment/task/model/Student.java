package horodenski.recruitment.task.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import horodenski.recruitment.task.exception.ObjectAlreadyInCollectionException;
import horodenski.recruitment.task.exception.ObjectNotInCollectionException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Size(min = 3 , message = "Name must have at least 3 digits")
    private String name;
    private String surname;
    @Min(value = 18, message = "Age should be at least 18")
    private Short age;
    @Email(message = "Provide valid email")
    private String email;
    @Enumerated(EnumType.STRING)
    private Major major;
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "Student_Teacher",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = {@JoinColumn(name = "teacher_id")}
    )
    @JsonIgnore
    @Builder.Default
    private Set<Teacher> teachers = new HashSet<>();

    public void addTeacher(Teacher teacher) {
        if (teachers.contains(teacher)) {
            throw new ObjectAlreadyInCollectionException("Teacher with id " + teacher.getId() + "already" +
                    "id list of teachers in student with id " + id);
        }
        teachers.add(teacher);
    }

    public void removeTeacher(Teacher teacher) {
        if (!teachers.contains(teacher)) {
            throw new ObjectNotInCollectionException("Teacher with id " + teacher.getId() +
                    " cannot be removed from student with id " + id + " teachers list, its not part of it");
        }

        teachers.remove(teacher);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Short getAge() {
        return age;
    }

    public void setAge(Short age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Major getMajor() {
        return major;
    }

    public void setMajor(Major major) {
        this.major = major;
    }

    public Set<Teacher> getTeachers() {
        return Set.copyOf(teachers);
    }

    public void setTeachers(Set<Teacher> teachers) {
        this.teachers = teachers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id.equals(student.id) && name.equals(student.name) && surname.equals(student.surname) && age.equals(student.age) && email.equals(student.email) && major == student.major;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, age, email, major);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", major=" + major +
                '}';
    }

}
