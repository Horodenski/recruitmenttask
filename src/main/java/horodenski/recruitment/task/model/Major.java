package horodenski.recruitment.task.model;

public enum Major {
    MATH,
    PHYSICS,
    BIOLOGY
}
