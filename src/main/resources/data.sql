INSERT INTO STUDENT(EMAIL, NAME, MAJOR, SURNAME, AGE) VALUES
('adam_nowak@gmail.com', 'Adam','MATH', 'Nowak', 22),
('piotr_polak@gmail.com', 'Piotr','PHYSICS', 'Polak', 21);

INSERT INTO TEACHER(EMAIL, NAME, SURNAME, SUBJECT, AGE) VALUES
('adam_woronowicz@gmail.com', 'Adam', 'Woronowicz', 'MATH', 45),
('piotr_michniewicz@gmail.com', 'Piotr', 'Michniewicz', 'PHYSICS', 55),
('wiktor_baran@gmail.com', 'Wiktor', 'Baran', 'BIOLOGY', 46),
('krzysztof_isak@gmail.com', 'Krzysztof', 'Isak', 'PHYSICS', 29),
('piotr_krasko@gmail.com', 'Piotr', 'Krasko', 'BIOLOGY', 32),
('aleks_nowak@gmail.com', 'Aleks', 'Nowak', 'PHYSICS', 66);


INSERT INTO STUDENT_TEACHER(STUDENT_ID, TEACHER_ID) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(2, 1),
(2, 2),
(2, 4),
(2, 6);



